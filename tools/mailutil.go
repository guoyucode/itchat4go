package tools

import (
	"log"
	"net/smtp"
	"strings"
	"time"
)

var (
	mailFrom     = "1234556@qq.com"              //发送邮件邮箱
	mailFromName = "1234556"                     //发送邮件的名字
	mailPassword = "password"                    //邮箱密码
	mailHost     = "smtp.mxhichina.com:25"       //发送邮件的地址
	mailTo       = "1234556@qq.com;76543@qq.com" //发送给哪些人
)

//发送邮件方法
func SendMail(subject, body string) {
	m := mailInfo{
		subject: subject,
		body:    body,
	}
	msgs <- m
}

type mailInfo struct {
	subject string
	body    string
}

var msgs = make(chan mailInfo, 500)

func init() {
	go func1()
}

//私有方法
func func1() {
	for true {
		timeout := make(chan bool, 1)
		select {
		case msg := <-msgs:
			go func() {
				time.Sleep(time.Second * 10)
				timeout <- true
			}()
			func2(msg.subject, msg.body)
		case <-timeout:
			log.Fatal("发送邮件超时")
		}
		time.Sleep(time.Second * 10)
	}
}

//私有方法
func func2(subject, body string) {
	subject = mailFromName + "(" + subject + ")" + time.Now().Format("_01-02 15:04:05.999")
	hp := strings.Split(mailHost, ":")
	auth := smtp.PlainAuth("", mailFrom, mailPassword, hp[0])
	var content_type string
	content_type = "Content-Type: text/" + "html" + "; charset=UTF-8"
	msg := []byte("To: " + mailTo + "\r\nFrom: " + mailFromName + "<" + mailFrom + ">\r\nSubject: " + subject + "\r\n" + content_type + "\r\n\r\n" + body)
	send_to := strings.Split(mailTo, ";")
	err := smtp.SendMail(mailHost, auth, mailFrom, send_to, msg)
	if err != nil {
		log.Fatal("发送邮件失败", err)
	}
}
