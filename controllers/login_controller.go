package controllers

import (
	"fmt"
	e "itchat4go/enum"
	m "itchat4go/model"
	s "itchat4go/service"
	"logs"
)

func Login(uuid string) (){
	for {
		fmt.Println("正在验证登陆... ...")
		status, msg := s.CheckLogin(uuid)
		if status == 200 {
			handle200(msg)
			return
		} else if status == 201 {
			logs.Info("请在手机上确认")
		} else if status == 408 {
			logs.Info("请扫描二维码")
		} else {
			logs.Info(msg)
		}
	}
}

func handle200(msg string)  {
	fmt.Println("登陆成功,处理登陆信息...")
	if  loginMap1, err := s.ProcessLoginInfo(msg); err != nil {
		logs.Error(err)
	}else{
		m.LoginM = &loginMap1
	}

	fmt.Println("登陆信息处理完毕,正在初始化微信...")
	if  err := s.InitWX(m.LoginM); err != nil {
		logs.Error(err)
	}

	fmt.Println("初始化完毕,通知微信服务器登陆状态变更...")
	if err := s.NotifyStatus(m.LoginM); err != nil {
		logs.Error(err)
	}

	fmt.Println("通知完毕,本次登陆信息：")
	fmt.Println(e.SKey + "\t\t" + m.LoginM.BaseRequest.SKey)
	fmt.Println(e.PassTicket + "\t\t" + m.LoginM.PassTicket)
}
