module itchat4go

require (
	github.com/astaxie/beego v1.10.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/json-iterator/go v1.1.5
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
	logs v1.0.1
)

replace (
	itchat4go v0.0.0 => ./
	logs v1.0.1 => ../logs
)
