package common

import (
	"github.com/json-iterator/go"
	"github.com/json-iterator/go/extra"
)

//json格式使用json-iterator提高性能
var Json jsoniter.API

func init() {

	//实例化
	Json = jsoniter.Config{
		UseNumber:   true,
		SortMapKeys: true,
	}.Froze()

	//此方法使用下划线的格式
	//extra.SetNamingStrategy(extra.LowerCaseWithUnderscores)

	//启动模糊模式: 可以容忍字符串和数字互转
	extra.RegisterFuzzyDecoders()
	//extra.SupportPrivateFields()
}
